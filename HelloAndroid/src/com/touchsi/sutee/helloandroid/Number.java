package com.touchsi.sutee.helloandroid;

public class Number {
	private int value;

	public interface OnNumberChangeListener {
		void onNumberChange(Number number);
	}

	private OnNumberChangeListener onNumberChangeListener;

	public void setOnNumberChangeListener(
			OnNumberChangeListener onNumberChangeListener) {
		this.onNumberChangeListener = onNumberChangeListener;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
		if (this.onNumberChangeListener != null) {
			this.onNumberChangeListener.onNumberChange(this);
		}
	}

}
