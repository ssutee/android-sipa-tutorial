package com.touchsi.sutee.flickrfetcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.touchsi.sutee.android.rest.RestClient;
import com.touchsi.sutee.android.rest.RestClient.OnRequestFinishListener;
import com.touchsi.sutee.android.rest.RestClient.RequestMethod;
import com.touchsi.sutee.flickrfetcher.FlickrPhotos.OnPhotosChangeListener;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class MainActivity extends Activity implements OnRequestFinishListener,
		OnPhotosChangeListener, OnRefreshListener {

	private static final String TAG = "MainActivity";
	private static final String FLICKR_API_KEY = "";
	private static final String FLICKR_GET_RECENT_PHOTOS_METHOD = "http://api.flickr.com/services/rest/?method=flickr.photos.getRecent&format=json&api_key=";
	private Handler mHandler = new Handler();
	private FlickrPhotos mPhotos = new FlickrPhotos();
	private PullToRefreshListView mListView;
	private PhotosAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mListView = (PullToRefreshListView) findViewById(R.id.listView);
		mAdapter = new PhotosAdapter(this) {
			@Override
			public Object getItem(int position) {
				return mPhotos.get(position);
			}

			@Override
			public int getCount() {
				return mPhotos.size();
			}
		};
		mListView.setAdapter(mAdapter);
		mPhotos.setOnPhotosChangeListener(this);
		mListView.setOnRefreshListener(this);
		reload();
	}

	public void fetch(View view) {
		reload();
	}

	private void reload() {
		RestClient restClient = new RestClient(FLICKR_GET_RECENT_PHOTOS_METHOD
				+ FLICKR_API_KEY, mHandler);
		restClient.setOnRequestFinishListener(this);
		restClient.execute(RequestMethod.GET);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onRequestFinish(RequestMethod method, int responseCode,
			String message, String response) {
		Pattern pattern = Pattern.compile("jsonFlickrApi\\((.*)\\)");
		Matcher matcher = pattern.matcher(response);
		if (matcher.find()) {
			try {
				mPhotos.clear();
				JSONObject jsonObject = new JSONObject(matcher.group(1));
				JSONArray photos = jsonObject.getJSONObject("photos")
						.getJSONArray("photo");
				for (int index = 0; index < photos.length(); ++index) {
					Photo photo = Photo
							.newInstance(photos.getJSONObject(index));
					mPhotos.insert(photo);
					Log.i(TAG, photo.getThumbUrl());
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		mListView.onRefreshComplete();
	}

	@Override
	public void onRequestFinishWithError(Exception e) {

	}

	@Override
	public void onPhotosChange(FlickrPhotos photos) {
		mAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onDestroy() {
		if (mAdapter != null) {
			mAdapter.stopImageLoader();
		}
		super.onDestroy();
	}

	@Override
	public void onRefresh() {
		reload();
	}
}
