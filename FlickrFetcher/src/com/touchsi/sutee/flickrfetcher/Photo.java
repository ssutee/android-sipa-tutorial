package com.touchsi.sutee.flickrfetcher;

import org.json.JSONException;
import org.json.JSONObject;

public class Photo {
	private String id;
	private int farm;
	private int server;
	private String secret;

	private String title;
	private String owner;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getFarm() {
		return farm;
	}

	public void setFarm(int farm) {
		this.farm = farm;
	}

	public int getServer() {
		return server;
	}

	public void setServer(int server) {
		this.server = server;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getThumbUrl() {
		return String.format(
				"http://farm%d.staticflickr.com/%d/%s_%s_s.jpg", farm,
				server, id, secret);
	}

	public static Photo newInstance(JSONObject jsonObject) {
		try {
			Photo photo = new Photo();
			photo.setFarm(jsonObject.getInt("farm"));
			photo.setServer(jsonObject.getInt("server"));
			photo.setId(jsonObject.getString("id"));
			photo.setTitle(jsonObject.getString("title"));
			photo.setSecret(jsonObject.getString("secret"));
			photo.setOwner(jsonObject.getString("owner"));
			return photo;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
