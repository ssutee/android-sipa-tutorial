package com.touchsi.sutee.flickrfetcher;

import java.util.ArrayList;

public class FlickrPhotos {
	private ArrayList<Photo> photos = new ArrayList<Photo>();

	public interface OnPhotosChangeListener {
		void onPhotosChange(FlickrPhotos photos);
	}

	private OnPhotosChangeListener onPhotosChangeListener;

	public void setOnPhotosChangeListener(
			OnPhotosChangeListener onPhotosChangeListener) {
		this.onPhotosChangeListener = onPhotosChangeListener;
	}

	public int size() {
		return photos.size();
	}

	public Photo get(int position) {
		return photos.get(position);
	}

	public void insert(Photo photo) {
		photos.add(photo);
		notifyPhotosChange();
	}

	public void clear() {
		photos.clear();
		notifyPhotosChange();
	}

	private void notifyPhotosChange() {
		if (onPhotosChangeListener != null) {
			onPhotosChangeListener.onPhotosChange(this);
		}
	}

}
