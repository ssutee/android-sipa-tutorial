package com.touchsi.sutee.dotdot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		TextView txtCoordX = (TextView) findViewById(R.id.txtCoordX);
		TextView txtCoordY = (TextView) findViewById(R.id.txtCoordY);
		Intent intent = getIntent();
		if (intent != null && intent.getExtras() != null) {
			int coordX = intent.getExtras().getInt(Constants.COORD_X_KEY);
			int coordY = intent.getExtras().getInt(Constants.COORD_Y_KEY);
			txtCoordX.setText(String.valueOf(coordX));
			txtCoordY.setText(String.valueOf(coordY));
		}
	}

	public void returnYes(View view) {
		Intent intent = new Intent();
		intent.putExtra(Constants.SECOND_ACTIVITY_RESULT_KEY, "Yes");
		setResult(Activity.RESULT_OK, intent);
		finish();
	}

	public void returnNo(View view) {
		Intent intent = new Intent();
		intent.putExtra(Constants.SECOND_ACTIVITY_RESULT_KEY, "No");
		setResult(Activity.RESULT_OK, intent);
		finish();
	}

}
