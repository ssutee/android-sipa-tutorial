package com.touchsi.sutee.dotdot;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.touchsi.sutee.dotdot.Dots.OnDotsChangeListener;
import com.touchsi.sutee.dotdot.DotsView.DotsViewDataSource;
import com.touchsi.sutee.dotdot.DotsView.OnDotsTouchListener;

public class MainActivity extends Activity implements OnDotsChangeListener,
		OnItemClickListener, OnDotsTouchListener {

	private static final int MAX_COORD_Y = 300;
	private static final int MAX_COORD_X = 300;
	private static final int MENU_CLEAR_ITEM = 1001;
	private static final int MENU_DELETE_ITEM = 1002;
	private static final int MENU_EDIT_ITEM = 1003;
	private static final int SECOND_ACTIVITY_REQUEST = 2001;
	private Dots mDots;
	private Random mGenerator = new Random();
	private ListView mListView;
	private DotListAdapter mAdapter;
	private DotsView mDotsView;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_CLEAR_ITEM:
			mDots.clear();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, MENU_CLEAR_ITEM, Menu.NONE,
				getString(R.string.clear));
		return true;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case MENU_DELETE_ITEM:
			confirmDelete(info.position);
			return true;
		case MENU_EDIT_ITEM:
			editDot(info.position);
		}
		return super.onContextItemSelected(item);
	}

	private void editDot(final int position) {
		View view = getLayoutInflater().inflate(R.layout.edit_dialog, null);
		final EditText edtCoordX = (EditText) view.findViewById(R.id.edtCoordX);
		final EditText edtCoordY = (EditText) view.findViewById(R.id.edtCoordY);
		Dot dot = mDots.get(position);
		edtCoordX.setText(String.valueOf(dot.getCoordX()));
		edtCoordY.setText(String.valueOf(dot.getCoordY()));
		new AlertDialog.Builder(this).setTitle("Edit").setView(view)
				.setPositiveButton(R.string.yes, new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int coordX = Integer.parseInt(edtCoordX.getText()
								.toString());
						int coordY = Integer.parseInt(edtCoordY.getText()
								.toString());
						mDots.edit(position, coordX, coordY);
					}
				}).setNegativeButton(R.string.no, null).show();
	}

	private void confirmDelete(final int position) {
		new AlertDialog.Builder(this).setTitle("Confirm Delete")
				.setMessage("Are you sure?")
				.setPositiveButton(R.string.yes, new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mDots.delete(position);
					}
				}).setNegativeButton(R.string.no, null).show();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		menu.add(Menu.NONE, MENU_DELETE_ITEM, Menu.NONE, R.string.delete);
		menu.add(Menu.NONE, MENU_EDIT_ITEM, Menu.NONE, R.string.edit);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mDots = new Dots(this);
		mDotsView = (DotsView) findViewById(R.id.dotsView);
		mDotsView.setDataSource(new DotsViewDataSource() {
			public Dot getItem(int position) {
				return mDots.get(position);
			}

			public int getCount() {
				return mDots.size();
			}
		});

		mDotsView.setOnDotsTouchListener(this);

		mAdapter = new DotListAdapter(this) {
			public Object getItem(int position) {
				return mDots.get(position);
			}

			public int getCount() {
				return mDots.size();
			}
		};

		mListView = (ListView) findViewById(R.id.listView);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);

		mDots.setOnDotsChangeListener(this);
		registerForContextMenu(mListView);
	}

	public void randomDot(View view) {
		int coordX = mGenerator.nextInt(mDotsView.getWidth());
		int coordY = mGenerator.nextInt(mDotsView.getHeight());

		Dot dot = new Dot(coordX, coordY);
		mDots.insert(dot);
	}

	public void onDotsChange(Dots dots) {
		mAdapter.notifyDataSetChanged();
		mDotsView.invalidate();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case SECOND_ACTIVITY_REQUEST:
			if (resultCode == Activity.RESULT_OK) {
				Toast.makeText(
						this,
						data.getExtras().getString(
								Constants.SECOND_ACTIVITY_RESULT_KEY),
						Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}

	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		Dot dot = mDots.get(position);
		Intent intent = new Intent(this, SecondActivity.class);
		intent.putExtra(Constants.COORD_X_KEY, dot.getCoordX());
		intent.putExtra(Constants.COORD_Y_KEY, dot.getCoordY());
		startActivityForResult(intent, SECOND_ACTIVITY_REQUEST);
	}

	public void onDotsTouch(DotsView dotsView, int coordX, int coordY) {
		mDots.insert(new Dot(coordX, coordY));
	}
}
