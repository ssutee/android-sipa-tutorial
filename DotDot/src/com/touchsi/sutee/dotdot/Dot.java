package com.touchsi.sutee.dotdot;

import android.os.Parcel;
import android.os.Parcelable;

public class Dot implements Parcelable {
	private int coordX;
	private int coordY;

	public Dot(int coordX, int coordY) {
		this.coordX = coordX;
		this.coordY = coordY;
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	public int describeContents() {
		return 0;
	}

	public static final Parcelable.Creator<Dot> CREATOR = new Creator<Dot>() {

		public Dot createFromParcel(Parcel source) {
			return new Dot(source);
		}

		public Dot[] newArray(int size) {
			return new Dot[size];
		}

	};

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(coordX);
		dest.writeInt(coordY);
	}

	private Dot(Parcel parcel) {
		coordX = parcel.readInt();
		coordY = parcel.readInt();
	}
}
