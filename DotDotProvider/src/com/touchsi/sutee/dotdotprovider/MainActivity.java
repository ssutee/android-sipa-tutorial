package com.touchsi.sutee.dotdotprovider;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor>, OnCancelListener {

	protected static final String TAG = "MainActivity";
	private ListView mListView;
	private DotCursorAdapter mAdapter;
	private Dao<Dot> mDotDao;
	private Button btnRandom;
	private ProgressDialog mProgressDialog;
	private DotRandomTask mTask;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mProgressDialog = new ProgressDialog(MainActivity.this);
		mProgressDialog.setMessage("Please wait");
		mProgressDialog.setTitle("Processing");
		mProgressDialog.setCancelable(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setMax(100);
		mProgressDialog.setOnCancelListener(this);

		btnRandom = (Button) findViewById(R.id.btnRandom);
		mListView = (ListView) findViewById(R.id.listView);
		mAdapter = new DotCursorAdapter(this);
		mListView.setAdapter(mAdapter);
		mDotDao = new Dao<Dot>(Dot.class, this, DotProvider.DOT_CONTENT_URI);
		getSupportLoaderManager().initLoader(0, null, this);
		DotApplication app = (DotApplication) getApplication();
		app.getValue();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		return new CursorLoader(this, DotProvider.DOT_CONTENT_URI, null, null,
				null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursor) {
		mAdapter.swapCursor(null);
	}

	public void randomDot(View view) {
		mTask = new DotRandomTask(mDotDao) {
			@Override
			protected void onPostExecute(Void result) {
				btnRandom.setEnabled(true);
				mProgressDialog.dismiss();
			}

			@Override
			protected void onPreExecute() {
				btnRandom.setEnabled(false);
				mProgressDialog.setProgress(0);
				mProgressDialog.show();

			}

			@Override
			protected void onProgressUpdate(Integer... values) {
				Log.i(TAG, String.valueOf(values[0]));
				mProgressDialog.setProgress(values[0]);
			}
		};
		mTask.execute(100);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		if (mTask != null) {
			mTask.cancel(true);
		}
	}
}
