package com.touchsi.sutee.dotdotprovider;

import android.app.Application;

public class DotApplication extends Application {
	int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
